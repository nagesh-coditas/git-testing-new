import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../components/Login.vue';
import GetUserId from '../components/Admin/GetUserId.vue';
import User from '../components/Admin/User.vue';
import UserFormApril from '../components/User/UserFormApril.vue';
import AdminUserSelect from '../components/Admin/AdminUserSelect.vue';
import UserFormOct from '../components/User/UserFormOct.vue';
import SelectQuarter from '../components/UserSelectQuarter.vue';
import AddAdmin from '../components/Admin/AddAdmin.vue';

Vue.use(VueRouter)

const routes = [
  

  {
    path: '/login',
    name:'Login',
    component: Login
  },
  
  {
    path:'/getuser/:quarter',
    name:'GetUser',
    component:GetUserId,
    props:true
  },
  {
    path:'/user/:user',
    name:'User',
    component:User,
    props:true
  },
  {
    path:'/userformapril',
    name:'UserFormApril',
    component:UserFormApril,
    props:true,
  },
  {
    path:'/admin',
    name:'AdminUserSelect',
    component:AdminUserSelect
  },
  {
    path:'/userformoct',
    name:'UserFormOct',
    component:UserFormOct,
    props:true,
  },
  {
    path:'/selectquarter',
    name:'SelectQuarter',
    component:SelectQuarter
  },
  {
    path:'/addadmin',
    name:'AddAdmin',
    component:AddAdmin
  },
 
 
 
]

const router = new VueRouter({
  routes
})

export default router
