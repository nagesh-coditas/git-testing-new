import Vue from 'vue'
import Vuex from 'vuex'
// import aprildata from '../components/allJsonFiles/aprildata.json';
// import octdata from '../components/allJsonFiles/octdata.json';

import firebase from "firebase";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {

    loginButtonClicked:true,

    getUserInfoClicked: false,

    loginLoading: true,

    dataNotFound: false,

    octButton: false,
    aprilButton: false,

    adminDataButton: false,
    userDataButton: false,

    loginError: null,
    UserSelect: false,

    quarterSelect: null,

    showData: false,

    firebaseToken: null,

    validUser: false,

    selectedUserOct: [],
    selectedUserApril: [],


    //store data locally 
    octDataReceived: [],
    aprilDataReceived: [],
    adminsList: [],

    //logged details.
    userLoggedApril: [],
    userLoggedOct: [],
    adminLogged: [],
    adminLoggedGoogleData: [],

  },

  mutations: {

    onSuccess(state) {

      state.loginError = null;

      state.loginButtonClicked = false;
     
      state.adminLogged = []

      const provider = new firebase.auth.GoogleAuthProvider();

      firebase.auth().signInWithPopup(provider).then(async (profile) => {


        state.loginError = null;

        // console.log("profile", state.firebaseToken = profile.credential)
        // console.log("profile",  profile)

        // state.firebaseToken = profile.credential.accessToken

        state.octDataReceived = []
        state.aprilDataReceived = []
        state.adminsList = []

        await this.dispatch('getAdminsData')
        await this.dispatch('getOctData')
        await this.dispatch('getAprilData')


        for (const user in state.aprilDataReceived) {

          if (profile.additionalUserInfo.profile.given_name + ' ' + profile.additionalUserInfo.profile.family_name === state.aprilDataReceived[user]["candidate_Name"])             // you can use this - "Arnav Mediratta" 

          {
            state.loginLoading = true;

            state.userLoggedApril = [state.aprilDataReceived[user], user]

            state.validUser = true;

            state.showData = true;

            state.loginButtonClicked = true;

          }
          else {

          }

        }

        for (const user in state.octDataReceived) {

          if (profile.additionalUserInfo.profile.given_name + ' ' + profile.additionalUserInfo.profile.family_name === state.octDataReceived[user]["first_name"] + ' ' + state.octDataReceived[user]["last_name"])   // you can use this - "Arnav Mediratta"

          {
            state.loginLoading = true;

            state.userLoggedOct = [state.octDataReceived[user], user]

            state.validUser = true;

            state.showData = true;

            state.loginButtonClicked = true;

          }
          else {

          }

        }
        for (const admin in state.adminsList) {

          console.log("all admins", state.adminsList[admin]['email'])

          if (profile.additionalUserInfo.profile.email === state.adminsList[admin]['email']) {


            state.adminLogged = state.adminsList[admin]['email']

            console.log("loggedAdmin", state.adminLogged)

            state.adminLoggedGoogleData = { name: profile.additionalUserInfo.profile.given_name, surname: profile.additionalUserInfo.profile.family_name }

            state.validUser = true;

            state.showData = true;

            state.loginButtonClicked = true;

          }
          else {


          }

        }
        
      // console.log(state.validUser,state.showData ,state.loginButtonClicked)

      if(!(state.validUser && state.showData && state.loginButtonClicked)){
        state.loginError = "No data found"
      }
      else{
       
      }


        state.loginLoading = false;
      }).catch(err => {
        console.log("error", err.message)
        state.loginError = err.message
      })
       
    },
    onLogout(state) {
      firebase.auth().signOut().then(function () {
        console.log("Sign Out Successful")
        state.validUser = false;
      })
    },

    // setters

    setAdminsList(state, payload) {
      state.adminsList = payload;
    },

    setSelectedUserOct(state, payload) {
      state.selectedUserOct = payload
    },

    setSelectedUserApril(state, payload) {
      state.selectedUserApril = payload
    },

    setValidUser(state, payload) {   //used for setting login flag.
      state.validUser = payload
    },

    setUserSelect(state, payload) {
      state.UserSelect = payload
    },

    setUserButtonClicked(state) {
      state.userButtonClicked = "true";

    },



    //api data into local

    octData(state, payload) {

      let temp = [];

      for (const u in payload) {

        const usersList = payload[u]["oct2020"]

        for (const id in usersList) {

          temp.push({
            first_name: usersList[id]["What is your* first name*?"],
            last_name: usersList[id]["And what is your *last name*?"],
            empid: usersList[id]["What is your employee id? "],
            password: usersList[id]["Please enter given password"],
            your_email: usersList[id]["Please enter your email address "],
            hierarchy: usersList[id]["What is your hierarchy level within Coditas?"],
            total_exp: usersList[id]["What is your total years of experience "],
            job_titile: usersList[id]["What is your *job title*?"],
            quality_of_work: usersList[id]["Quality of Work"],
            job_knowledge: usersList[id]["Job Knowledge"],
            productivity: usersList[id]["Productivity"],
            communication: usersList[id]["Communication Skills"],
            creativity: usersList[id]["Creativity"],
            commitment: usersList[id]["Commitment"],
            goal_accomplish: usersList[id]["_Goal Accomplishment_"],
            rate_performance: usersList[id]["{{field:680e6ffea770322e}}, how would you *rate your job performance* overall?"],
            imp_achievements: usersList[id]["What do you consider to be your most important achievements of the past year?"],
            difficult_past: usersList[id]["What elements of your job did you find difficult last year"],
            most_imp_tasks: usersList[id]["What do you consider to be your most important aim or tasks in the past year? Rate yourself on how far you have achieved them"],
            how_many_goals: usersList[id]["How may goals would you like to set in this quarter (max 3) "],
            project_specific_goal: usersList[id]["What is your project-specific goal for the current quarter? "],
            key_matric_1: usersList[id]["Enter 'Key Metric-1' for your goal {{field:13ef2fe6-845e-4f9b-ab4f-280dcac44fb7}}"],
            key_matric_2: usersList[id]["Enter 'Key Metric-1' for your goal {{field:13ef2fe6-845e-4f9b-ab4f-280dcac44fb7}}"],
            key_matric_3: usersList[id]["Enter 'Key Metric-1' for your goal {{field:13ef2fe6-845e-4f9b-ab4f-280dcac44fb7}}"],
            org_prespective_goal: usersList[id]["From an organization perspective, what is your goal for the current quarter? "],
            key_matric_1_1: usersList[id]["Enter 'Key Metric-1' for your goal {{field:160c9b00-4b71-4098-93ca-b296cdb4fc5f}}"],
            key_matric_1_2: usersList[id]["Enter 'Key Metric-2' for your goal {{field:160c9b00-4b71-4098-93ca-b296cdb4fc5f}}"],
            key_matric_1_3: usersList[id]["Enter 'Key Metric-3' for your goal {{field:160c9b00-4b71-4098-93ca-b296cdb4fc5f}}"],
            personal_goal: usersList[id]["Do you have any personal goals for this quarter?"],
            trying_to_achive: usersList[id]["What is the goal that you are trying to achieve "],
            key_matric_1_1_1: usersList[id]["Enter 'Key Metric-1' for your goal {{field:72b649c5-bc96-44dd-be85-69bc9fc26697}}"],
            key_matric_1_1_2: usersList[id]["Enter 'Key Metric-2' for your goal {{field:72b649c5-bc96-44dd-be85-69bc9fc26697}}"],
            key_matric_1_1_3: usersList[id]["Enter 'Key Metric-3' for your goal {{field:72b649c5-bc96-44dd-be85-69bc9fc26697}}"],
            surpervisor: usersList[id]["What is your *supervisor's first name*?"],
            surpervisor_lastname: usersList[id]["And what is your *supervisor's last name*?"],
            email_address: usersList[id]["Please enter your supervisor's email address "],
            org_skills: usersList[id]["Organization Skills"],
            leadership: usersList[id]["Leadership Skills"],
            overall_rate: usersList[id]["{{field:680e6ffea770322e}}, how would you *rate* *{{field:38f79c46299f6e37}}**'s job performance and management ability* overall?"],
            strong_point: usersList[id]["What are {{field:38f79c46299f6e37}}'s strong *points*?"],
            weak_point: usersList[id]["And what are {{field:38f79c46299f6e37}}'s weak *points*?"],
            submitted: usersList[id]["Submitted At"],
            token: usersList[id]["Token"]
          })

          state.octDataReceived = temp;
        }
      }

    },

    aprilData(state, payload) {

      let temp = [];

      for (const u in payload) {

        const usersList = payload[u]["april2021"]

        for (const id in usersList) {

          temp.push({
            userid: usersList[id]["What is your Employee Id"],
            candidate_Id: usersList[id]["What is your Employee Id"],
            candidate_Name: usersList[id]["What is your Name"],
            first_review: usersList[id]["Is this your first review"],
            personal_goals_for_last_quarter: usersList[id]["What was the personal Goal that you had set in the last quarter"],
            extend_of_meet_first_goal: usersList[id]["To what extent did you meet the individual goals set in the last Employee Performance Review?"],
            project_specific_goal_for_last_quarter: usersList[id]["What was the Project Specific Goal that you had set in the last quarter"],
            extend_for_project_specific_goal: usersList[id]["To what extent did you meet the Organizational goals set in the last Employee Performance Review?"],
            organizational_goal: usersList[id]["What was the Organizational Goal that you had set in the last quarter"],
            extend_for_organizational_goal: usersList[id]
            ["To what extent did you meet the Organizational goals set in the last Employee Performance Review?"],
            aspirational_goal: usersList[id]["What is the Aspirational Goal that you want to achieve this quarter"],
            firststep_for_aspirational_goal: usersList[id]["What is the first step for this goal"],
            secondstep_for_aspirational_goal:
              usersList[id]["What is the second step for this goal"],
            thirdstep_for_aspirationl_goal: usersList[id]["What is the third step for this goal"],
            commitment_goal_for_this_quarter: usersList[id]["What is the Commitment Goal that you want to achieve this quarter"],
            final_comments_to_raise: usersList[id]["Do you have any final comments you would like to raise?"],
            submitted_at: usersList[id]["Submitted At"],
            user_token: usersList[id]["Token"]
          })

          state.aprilDataReceived = temp
        }

      }
    },

    adminsData(state, payload) {

      const temp = []

      for (const id in payload) {

        for (const data in payload[id]) {

          // console.log("2",pms[id][data]["email"])
          const admins = {
            "id": id,
            "email": payload[id][data]["email"]
          }
          temp.push(admins)

        }

      }


      state.adminsList = temp
      // console.log(state.adminsList)

    }


  },

  actions: {

    async getOctData(context) {

      const firebaseToken = context.getters.getFirebaseToken;

      // const response = await fetch(`https://vue-pms-webapp-default-rtdb.firebaseio.com/pms-vue-octdata.json?access_token=` + firebaseToken)
      const response = await fetch(`https://vue-pms-webapp-default-rtdb.firebaseio.com/pms-vue-octdata.json`)

      if (!response.ok) {
        //  error
      }
      const responseData = await response.json();

      context.commit('octData', responseData)
      //  console.log(`DataOutput-`,data)


    },
    async getAprilData(context) {

      // const firebaseToken = context.getters.getFirebaseToken;

      // const response = await fetch(`https://vue-pms-webapp-default-rtdb.firebaseio.com/pms-vue-aprildata.json?access_token=` + firebaseToken)
      const response = await fetch(`https://vue-pms-webapp-default-rtdb.firebaseio.com/pms-vue-aprildata.json`)

      if (!response.ok) {
        //error
      }

      const responseData = await response.json();

      context.commit('aprilData', responseData)

    },

    async getAdminsData(context) {

      // const firebaseToken = context.getters.getFirebaseToken;

      // console.log("getAdminsData Called")

      // const response = await fetch(`https://vue-pms-webapp-default-rtdb.firebaseio.com/pms-vue-admins.json?access_token=` + firebaseToken)
      const response = await fetch(`https://vue-pms-webapp-default-rtdb.firebaseio.com/pms-vue-admins.json`)

      const responseData = await response.json();

      if (!response.ok) {
        const error = new Error(responseData.message || 'Failed to get Admins')
        throw error;
      }

      context.commit('adminsData', responseData)

    },

    async addAdmin(context, payload) {

      // const firebaseToken = context.getters.getFirebaseToken;

      const adminId = Date.now()

      // console.log(adminId)

      const admin = {
        id: adminId,
        email: payload.email
      }

      const response = await fetch(`https://vue-pms-webapp-default-rtdb.firebaseio.com/pms-vue-admins/${adminId}.json`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(admin),
      });

      const responseData = await response.json();

      if (!response.ok) {

        const error = new Error(responseData.message || 'Failed to Add Admins')
        throw error;

        //error
      } else {

        context.commit('adminsData', admin)

      }

    },
    async removeAdmin(context, payload) {

      // const firebaseToken = context.getters.getFirebaseToken;

      const adminId = payload.id;

      console.log("payload", adminId)

      const response = await fetch(`https://vue-pms-webapp-default-rtdb.firebaseio.com/pms-vue-admins/${adminId}.json`, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(),
      });
      const responseData = await response.json();

      if (!response.ok) {
        const error = new Error(responseData.message || 'Failed to Delete')
        console.log("removeAdmin", error, response)
        throw error;

        //error
      } else {

        const responseData = this.dispatch('getAdminsData')

        context.commit('adminsData', responseData)

        console.log("response", responseData)

        alert(`${payload.email} Deleted.`)

      }

    },
  },


  getters: {

    getAdminsList(state) {
      return state.adminsList;
    },
    getSelectedUserOct(state) {
      return state.selectedUserOct;
    },
    getSelectedUserApril(state) {
      return state.selectedUserApril;
    },
    getUserSelect(state) {
      return state.UserSelect
    },
    getFirebaseToken(state) {
      return state.firebaseToken
    }

  },


})
